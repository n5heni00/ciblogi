<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Blogi extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Blogi_Model');
        $this->load->helper('url');
        $this->load->helper('form');
        
    }

    public function index() {

        $config['base_url'] = site_url('blogi/index');
        $data['kirjoitukset'] = $this->Blogi_Model->hae_kaikki();
        $data['navigation'] = 'nav/navigation_view';
        $data['main_content'] = 'kirjoitus/kirjoitukset_view';
        $this->load->view('template', $data);
    }
    
    public function siirry($id) {
       $data['kirjoitukset'] = $this->Blogi_Model->hae($id);
       $data['navigation'] = 'nav/navigation_view';
       $data['main_content'] = 'kirjoitus/kirjoitus_view';
       $this->load->view('template',$data);
    }
    
    public function lisaa() {
        $data = array(
            'id' => '',
            'otsikko' => '',
            'paivays' => '',
            'teksti' => ''
        );
        $data['main_content'] = 'kirjoitus/lisaakirjoitus_view';
        $data['navigation'] = 'nav/navigation_view';
        $this->load->view('template', $data);
    }
    
    public function kommentoi($id) {
        $data['kirjoitukset'] = $this->Blogi_Model->hae($id);
        $data = array(
            'id' => '',
            'kirjoitus_id' => $id,
            'paivays' => '',
            'teksti' => $this->input->post('teksti'),
            'kayttaja_id' => 1
        );
        $this->Blogi_Model->lisaaKommentti($data);
        redirect('blogi/lisaa', 'refresh');
    }

    public function poista($id) {
        $this->Asiakas_model->poista(intval($id));
        $this->index();
    }

    public function tallenna() {

        $data = array(
            'otsikko' => $this->input->post('otsikko'),
            'teksti' => $this->input->post('teksti'),
            'kayttaja_id' => 1
        );
        $this->Blogi_Model->lisaa($data);
        redirect('blogi/index', 'refresh');
        
    }
}