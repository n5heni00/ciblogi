<?php

class Blogi_Model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }
    
    public function hae_kaikki() {
        
        $this->db->select();
        $this->db->from('kirjoitus');
//        $this->db->join('kayttaja', 'kayttaja.id = kirjoitus.kayttaja_id');
        $kysely = $this->db->get();
        return $kysely->result();
    }
    
    public function hae($id) {
        $this->db->where('id',$id);
        $kysely= $this->db->get('kirjoitus');
        return $kysely->row($id);
    }

    public function lisaa($data) {
        $this->db->insert('kirjoitus', $data);
        return $this->db->insert_id();
    }
    
    public function lisaaKommentti($data) {
        $this->db->insert('kommentti', $data);
        return $this->db->insert_id();
    }
}
