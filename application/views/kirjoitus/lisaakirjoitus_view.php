<div class="container">
    <form action="<?php echo site_url('blogi/tallenna')?>" method="post">
        <br>
        <h1>Lisää kirjoitus</h1>
        <div class="form-group">
            <label for="otsikko">Otsikko</label>
            <input type="text" class="form-control" name="otsikko">
        </div>
        <div class="form-group">
            <label for="teksti">Teksti</label>
            <textarea type="text" rows="5" class="form-control" name="teksti"></textarea>
        </div>
        <input type="submit"><br><br><br>
        <a href="<?php echo site_url('blogi/index')?>">Takaisin etusivulle.</a>
    </form>
    <?php
    echo validation_errors();
    ?>
</div>