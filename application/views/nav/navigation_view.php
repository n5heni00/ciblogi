<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href="#">Blogi</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
    <div class="navbar-nav">
      <a class="nav-item nav-link" href="<?php echo site_url('blogi/index')?>">Etusivulle</a>
      <a class="nav-item nav-link" href="<?php echo site_url('blogi/lisaa')?>">Lisää kirjoitus</a>
      <a class="nav-item nav-link" href="#">Kirjaudu ulos</a>
      <a class="nav-item nav-link" href="#">Kirjaudu sisään</a>
    </div>
  </div>
</nav>